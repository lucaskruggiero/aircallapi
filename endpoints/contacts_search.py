from dataclasses import dataclass
from typing import List, Optional
from dacite import from_dict
from utils.requests import Request

import allure
import config 

@dataclass
class Email:
    id: int
    label: str
    value: str

@dataclass
class Phone_numbers:
    id: int
    label: str
    value: str

@dataclass
class Contacts:
    id: int
    direct_link: str
    first_name: Optional[str]
    last_name: Optional[str]
    company_name: Optional[str]
    information: Optional[str]
    is_shared: bool
    created_at: int
    updated_at: int
    emails: List[Email]
    phone_numbers: List[Phone_numbers]

@dataclass
class Meta:
    count: int
    total: int
    current_page: int
    per_page: int
    next_page_link: Optional[str]
    previous_page_link: Optional[str]

@dataclass
class Search:
    contacts: List[Contacts]
    meta: Meta

class Contacts_Search:
    @allure.step('Search for contact with phone number: "{phone_number}"')
    def search_contacts(self, phone_number=''):
        request = Request()
        response = request.get(config.endpoints['contacts_search'] + '?order=asc&order_by=created_at&phone_number={}'.format(phone_number))
        try:
            search_dict = from_dict(data_class=Search, data=response.body)
        except Exception:
            search_dict = {}
        return response, search_dict

