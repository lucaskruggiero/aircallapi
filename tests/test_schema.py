import config
import allure
import pytest

from assertpy.assertpy import assert_that
from cerberus import Validator
from utils.requests import Request

@allure.title("/contacts/search filter: Validate schema")
@pytest.mark.parallel
def test_schema_validation():
    schema = {
        'contacts':{
            'type': 'list',
            'schema': {
                'type': 'dict',
                'schema':{
                    'id':{'type': 'integer'},
                    'direct_link':{'type': 'string'},
                    'first_name':{'type': 'string', 'nullable': True},
                    'last_name':{'type': 'string', 'nullable': True},
                    'company_name':{'type': 'string', 'nullable': True},
                    'information':{'type': 'string', 'nullable': True},
                    'is_shared':{'type': 'boolean'},
                    'created_at':{'type': 'integer'},
                    'updated_at':{'type': 'integer'},
                    'emails':{
                        'type': 'list',
                        'schema': {
                            'type': 'dict',
                            'schema':{
                                'id':{'type': 'integer'},
                                'label':{'type': 'string'},
                                'value':{'type': 'string'},
                            }
                        }
                    },
                    'phone_numbers':{
                        'type': 'list',
                        'schema': {
                            'type': 'dict',
                            'schema':{
                                'id':{'type': 'integer'},
                                'label':{'type': 'string'},
                                'value':{'type': 'string'},
                            }
                        }
                    }
                }
            }
        },
        'meta':{
            'type': 'dict',
                'schema': {
                    'count':{'type': 'integer'},
                    'total':{'type': 'integer'},
                    'current_page':{'type': 'integer'},
                    'per_page':{'type': 'integer'},
                    'next_page_link':{'type': 'string', 'nullable': True},
                    'previous_page_link':{'type': 'string', 'nullable': True}
                }
            }
    }
    request = Request()
    response = request.get(config.endpoints['contacts_search'] + '?order=asc&order_by=created_at&phone_number=')
    validator = Validator(schema)
    is_valid = validator.validate(response.body)
    assert_that(is_valid, description=validator.errors).is_true()
