import pytest
import allure
import config 

from assertpy.assertpy import assert_that, soft_assertions
from endpoints.contacts_search import Contacts_Search
from utils.requests import Request

headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Basic {}'.format('not a valid token')
}

@allure.title("/contacts/search filter: Test authorization")
@allure.description("Validate if user can`t get response without proper authorization")
@pytest.mark.parallel
def test_search_contacts_authorization():
    phone_number = '+33652556756'
    request = Request()
    response = request.get(config.endpoints['contacts_search'] + '?order=asc&order_by=created_at&phone_number={}'.format(phone_number), headers)
    with soft_assertions():
        assert_that(response.status_code).is_equal_to(401)
        assert_that(response.body).is_equal_to(request.error_response)
