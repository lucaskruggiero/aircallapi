import pytest
import allure
from assertpy.assertpy import assert_that
from endpoints.contacts_search import Contacts_Search
from deepdiff import DeepDiff

@allure.title("/contacts/search filter: Test diff between before and after release")
@allure.description("To validate if the release haven't afected any values or structures after release")
@pytest.mark.serial
def test_search_contacts_deep_diff():
    phone_number = '+33652556756'
    contacts = Contacts_Search()
    search, _ = contacts.search_contacts(phone_number)
    print(search.headers)
    base_response = {'contacts': [{'id': 34069295, 'direct_link': 'https://api.aircall.io/v1/contacts/34069295', 'first_name': 'Merl', 'last_name': 'Rowland', 'company_name': 'Keebler, Kuvalis and McLaughlin', 'information': '', 'is_shared': True, 'created_at': 1583745888, 'updated_at': 1614184974, 'emails': [{'id': 76594398, 'label': 'Work', 'value': 'mandatory_field@example.com'}], 'phone_numbers': [{'id': 60650277, 'label': 'Mobile', 'value': '+33652556756'}]}], 'meta': {'count': 1, 'total': 1, 'current_page': 1, 'per_page': 20, 'next_page_link': None, 'previous_page_link': None}}
    deep_diff = DeepDiff(base_response, search.body, ignore_order=True)

    assert_that(deep_diff).is_empty()

test_search_contacts_deep_diff()