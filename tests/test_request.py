import requests
import pytest
import allure
from assertpy.assertpy import assert_that, soft_assertions
from endpoints.contacts_search import Contacts_Search
from utils.requests import Request

@allure.title("/contacts/search filter: Test contact fields")
@allure.description("Validate if contact fields are proper")
@pytest.mark.parallel
def test_search_contacts_fields():
    phone_number = '+33652556756'
    contacts = Contacts_Search()
    response, search = contacts.search_contacts(phone_number)
    
    with soft_assertions():
        # Arguably selecting the first occurrence
        first_contact = search.contacts[0]
        assert_that(response.status_code).is_equal_to(requests.codes.ok)
        assert_that(first_contact.id).is_equal_to(34069295)
        assert_that(first_contact.direct_link).is_equal_to('https://api.aircall.io/v1/contacts/34069295')
        assert_that(first_contact.is_shared).is_true()
        # Would rather to: create resource on setup and get date, then use ´is_between(setup_date, test_date)´
        assert_that(first_contact.created_at).is_between(1583745881, 1583745889)
        assert_that(first_contact.updated_at).is_between(1614184971, 1614184975)
        assert_that(first_contact.emails).is_type_of(list)
        assert_that(first_contact.phone_numbers).is_type_of(list)

@allure.title("/contacts/search filter: Test emails fields")
@allure.description("Validate if emails fields are proper")
@pytest.mark.parallel
def test_search_emails_fields():
    phone_number = '+33652556756'
    contacts = Contacts_Search()
    response, search = contacts.search_contacts(phone_number)
    
    with soft_assertions():
        # Arguably selecting the first occurrence
        emails = search.contacts[0].emails[0]
        assert_that(response.status_code).is_equal_to(requests.codes.ok)
        assert_that(emails.id).is_equal_to(76594398)
        assert_that(emails.label).is_equal_to('Work')
        assert_that(emails.value).is_equal_to('mandatory_field@example.com')

@allure.title("/contacts/search filter: Test phone_number fields")
@allure.description("Validate if phone_numbers fields are proper")
@pytest.mark.parallel
def test_search_phone_numbers_fields():
    phone_number = '+33652556756'
    contacts = Contacts_Search()
    response, search = contacts.search_contacts(phone_number)
    
    with soft_assertions():
        # Arguably selecting the first occurrence
        first_contact = search.contacts[0].phone_numbers[0]
        assert_that(response.status_code).is_equal_to(requests.codes.ok)
        assert_that(first_contact.id).is_equal_to(60650277)
        assert_that(first_contact.label).is_equal_to('Mobile')
        assert_that(first_contact.value).is_equal_to(phone_number)

@allure.title("/contacts/search filter: Test meta fields")
@allure.description("Validate if meta fields are proper")
@pytest.mark.parallel
def test_search_meta_fields():
    phone_number = '+33652556756'
    contacts = Contacts_Search()
    response, search = contacts.search_contacts(phone_number)
    
    with soft_assertions():
        assert_that(response.status_code).is_equal_to(requests.codes.ok)
        assert_that(search.meta.count).is_equal_to(1)
        assert_that(search.meta.total).is_equal_to(1)
        assert_that(search.meta.current_page).is_equal_to(1)
        assert_that(search.meta.per_page).is_equal_to(20)
        assert_that(search.meta.next_page_link).is_none()
        assert_that(search.meta.previous_page_link).is_none()

@allure.title("/contacts/search filter: Test if direct_link redirects accordingly")
@allure.description("Validate direct_link resource is the same as the one got from the search")
@pytest.mark.parallel
def test_search_direct_link():
    request = Request()
    phone_number = '+33652556756'
    contacts = Contacts_Search()
    response_search, search = contacts.search_contacts(phone_number)
    
    response_direct_link = request.get(search.contacts[0].direct_link)
    with soft_assertions():
        assert_that(response_search.status_code).is_equal_to(requests.codes.ok)
        assert_that(response_direct_link.status_code).is_equal_to(requests.codes.ok)
        assert_that(response_search.body['contacts'][0]).is_equal_to(response_direct_link.body['contact'])

@allure.title("/contacts/search filter: Test status codes for diferent phone_numbers")
@pytest.mark.parallel
@pytest.mark.parametrize(
    "phone_number,status_code", [
        # phone             # status code
        ('+33652556756', requests.codes.ok), 
        # nonexistent       # status code
        ('+33652556757', requests.codes.ok),
        # not a #           # status code
        ('test', requests.codes.ok) # Arguably
        ])
def test_status_code(phone_number, status_code):
    contacts = Contacts_Search()
    response_search, _ = contacts.search_contacts(phone_number)
    assert_that(response_search.status_code).is_equal_to(status_code)
