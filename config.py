import base64
import json

BASE_URI = 'https://api.aircall.io/v1/'

with open("apiauth.json") as jsonFile:
    jsonObject = json.load(jsonFile)
    jsonFile.close()

API_ID = jsonObject['api_id']
API_TOKEN = jsonObject['api_token']
AUTH_STRING = '{}:{}'.format(API_ID, API_TOKEN)
# TODO: whats this? encode encode decode?
AUTH = base64.b64encode(AUTH_STRING.encode('ascii')).decode("utf-8")

endpoints = {
    'contacts_search': '{}contacts/search/'.format(BASE_URI),
    'contacts': '{}contacts/'.format(BASE_URI)
}