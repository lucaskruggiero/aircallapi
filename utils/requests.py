from dataclasses import dataclass

import requests
import config 
import allure

@dataclass
class Response:
    headers: dict
    status_code: int
    body: dict

default_headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Basic {}'.format(config.AUTH)
}

error_response = {
    'error': 'Unauthorized', 
    'troubleshoot': 'Check your API key'
    }

class Request:
    @allure.step   
    def get(self, endpoint, headers=default_headers) :
        response = requests.get(endpoint, headers=headers)
        allure.attach(response.text)
        return self.__get_response(response)

    def __get_response(self, response):
        headers = response.headers
        status_code = response.status_code

        try:
            body = response.json()
        except Exception:
            body = {}

        return Response(
            headers, status_code, body
        )
    error_response = {
            'error': 'Unauthorized', 
            'troubleshoot': 'Check your API key'
        }
