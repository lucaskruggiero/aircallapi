# Test API Exercise #

This repository contains automated tests and a test plan with test cases.

### Context: ###

As an agent, your account gives you access to all the contacts of the company. Here, we’re looking for a contact of which the number is: +33652556756

The API url given gives you information about contacts having a specific number: +33652556756

### Tools: ###

1. pipenv -> Package Control
1. pytest -> Test framework
1. requests
1. cerberus -> Schema validation
1. allure-pytest -> Report framework
1. assertpy -> Assertion framework
1. dacite -> Transform JSON to dict
1. dataclasses
1. typing
1. pytest-xdist -> Parallel testing
1. deepdiff -> Diff before and after release


### How to install: ###

To ease out installation and better manage packages, I decided to use ´pipenv´. To install it [follow steps on their documentation](https://pipenv.pypa.io/en/latest/install/#installing-pipenv).

#### Create a home directory ####
    $ mkdir ~/.virtualenvs

#### Create a json file with your tokens called apiauth.json ####

    # apiauth.json
    {
        "api_id": <your api id>,
        "api_token": <you api token>
    }

#### Add below in .zshrc or .bash_profile (if on mac/linux) or in your windows system variables ####
    export WORKON_HOME=~/virtualenvs
    export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python3
    export LANG=en_US.UTF-8
    export LC_ALL=en_US.UTF-8

#### Source the above changes ####
    $ source .zshrc
or

    $ source .bash_profile

#### Install depedencies ####
    $ pipenv install

#### Start the new virtualenv ####
    $ pipenv shell

#### Installing allure shell ####
To install [follow steps on their documentation](https://docs.qameta.io/allure/#_installing_a_commandline)

### Running tests ###

To make test execution faster, some tests run in parallel and you can run the with the command:

    $ py.test --alluredir=allure-results -m parallel -n 10
You can change ´10´ for another number to determine the amount of parallel executions, changing for ´auto´ will use systems best effort.
To run serial test, run this command:

    $ py.test --alluredir=allure-results -m serial 

### Checking the report ###
I am using allure to get a report. To see them, simply execute:

    $ allure serve
and it will be open in a browser.

## [Test Plan](https://bitbucket.org/lucaskruggiero/aircallapi/src/main/Test%20Plan/) ##