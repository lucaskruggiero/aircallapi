# Test Plan for /contacts/search filtering #

## Summary ##

In order to test this feature, some assumptions were made about how it should work and how it is used, those assumptions will be pointed out on test cases.
Test cases that are automated will be marked as so.
Tools used for automation are described at [README](https://bitbucket.org/lucaskruggiero/aircallapi/src/main/). For manual test I have used Postman.

## Context ##

As an agent, your account gives you access to all the contacts of the company. Here, we’re looking for a contact of which the number is: +33652556756

The API url given gives you information about contacts having a specific number: +33652556756

## Schema and diff ##
### Schema ###
I have added an automated schema test running against ´contacts/search´ validating the schema with different resources having different empty fields. This is one of the assumptions, I haven't go through pages to check other nullable fields, so I am taking fields that are null just from the first page as an example. So this may fail if other resource is added.

### Diff ###
The idea of this technique is to compare the result of the same resources on different environments or different states(before and after release). So this would run on the pipeline or on staging and production environment if they have the same data(or part of the same data, as a unaccessible testing resource on production, as an example)

## Out of scope ##

1. Performance tests
1. Security tests
1. Throttling
1. Permissions

## Basic Performance ##
No test should take more than 10s to execute, check ´X-Runtime´ on header

## Exploratory testing ##

There's not much to test on this context, but running some tests with a few strings from [List of naughty strings](https://github.com/minimaxir/big-list-of-naughty-strings) may find some hidden bugs.

## [Test Cases](https://bitbucket.org/lucaskruggiero/aircallapi/src/main/Test%20Plan/Test%20Cases/) ##