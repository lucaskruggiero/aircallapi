# Header

Date should have following structure:

    'Date': 'Sun, DD Jan YYYY HH:MM:SS GMT'
    
X-Request-Id should be a string like the following:

    'X-Request-Id': '7818dd9e-eba2-4f49-ab91-eca87070cbc2'
    
Time server took to respond the request

    'X-Runtime': '0.038826'

Fields:

    'Content-Type': 'application/json
    charset=utf-8'
    'Transfer-Encoding': 'chunked'
    'Connection': 'keep-alive'
    'Server': 'nginx'
    'X-Frame-Options': 'SAMEORIGIN'
    'X-XSS-Protection': '1; mode=block'
    'X-Content-Type-Options': 'nosniff'
    'X-Download-Options': 'noopen'
    'X-Permitted-Cross-Domain-Policies': 'none'
    'Referrer-Policy': 'strict-origin-when-cross-origin'
    'Cache-Control': 'max-age=0, private, must-revalidate'
    'Strict-Transport-Security': 'max-age=31536000; includeSubDomains'
    'Vary': 'Origin'
    'Content-Encoding': 'gzip'
    



ETag should be a hash
    'ETag': 'W/"1abde102b1a3793ded05ec6318e2bf39"'